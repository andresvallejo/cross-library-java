package com.crossover.techtrial.dto;

import com.crossover.techtrial.model.Member;

public class MemberDTO {

	private Member member;
	private Long booksNumber;
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public Long getBooksNumber() {
		return booksNumber;
	}
	public void setBooksNumber(Long booksNumber) {
		this.booksNumber = booksNumber;
	}
	
	public MemberDTO(Member member, Long booksNumber) {
		super();
		this.member = member;
		this.booksNumber = booksNumber;
	}
	
}
