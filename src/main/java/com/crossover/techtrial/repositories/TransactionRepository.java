/**
 * 
 */
package com.crossover.techtrial.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.crossover.techtrial.dto.MemberDTO;
import com.crossover.techtrial.model.Book;
import com.crossover.techtrial.model.Member;
import com.crossover.techtrial.model.Transaction;

/**
 * @author crossover
 *
 */
@RestResource(exported = false)
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

	@Query("select count(*) from Transaction t where t.book = :book and t.dateOfReturn is null")
	public int findIssuedBook(@Param("book") Book book);
	
	@Query("select count(*) from Transaction t where t.member = :member and t.dateOfReturn is null")
	public int memberIssuedBooks(@Param("member") Member member);
	
	@Query("select new com.crossover.techtrial.dto.MemberDTO (t.member, count(*)) from Transaction t where t.dateOfIssue is not null and t.dateOfReturn is not null and t.dateOfIssue between :startTime and :endTime and t.dateOfReturn between :startTime and :endTime group by t.member order by count(*) desc")
	public List<MemberDTO> topMemberIssuedBooks(@Param("startTime") LocalDateTime starTime, @Param("endTime") LocalDateTime endTime, Pageable pageable);
	
}
